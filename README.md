[![Latest Release](https://gitlab.com/fton/keyword-parser/-/badges/release.svg)](https://gitlab.com/fton/keyword-parser/-/releases)
[![pipeline status](https://gitlab.com/fton/keyword-parser/badges/main/pipeline.svg)](https://gitlab.com/fton/keyword-parser/-/commits/main)
[![coverage report](https://gitlab.com/fton/keyword-parser/badges/main/coverage.svg)](https://gitlab.com/fton/keyword-parser/-/commits/main)
# Description
This package provide keyword parser using with [combine].
# Usage
```rust
use ::keyword_parser::transition::*;
use ::transition_table::*;
use ::combine::EasyParser;

const UNITS_TBL: [(&str, u8); 27] = [
    // L
    ("m", 1),
    ("inch", 2),
    ("もしもし", 3),
    // M
    ("g", 4),
    ("lb", 5),
    // T
    ("s", 6),
    ("Hz", 7),
    // θ
    ("K", 8),
    ("\u{00B0}R", 9), // °R
    ("\u{00B0}F", 10), // °F
    ("\u{2109}", 11), // ℉
    ("\u{00B0}C", 12), // °C
    ("\u{2103}", 13), // ℃
    // N
    ("mol", 14),
    // I
    ("A", 15),
    // J
    ("cd", 16),
    // Force
    ("N", 17),
    ("gf", 18),
    ("lbf", 19),
    // Pressure
    ("Pa", 20),
    ("ata", 21),
    ("psi", 22),
    // Energy
    ("J", 23),
    ("cal", 24),
    ("Btu", 25),
    ("W", 26),
    ("Wh", 27),
];

let tree = Entry::<char, _>::new(UNITS_TBL.iter());
let tbl: Vec<Transition<_, _, _>> = tree.into();
let mut p = keyword(&tbl);
let i = p.easy_parse("もしもし").map(|x| x.0).unwrap();

assert_eq!(UNITS_TBL[i].1, 3);
```
# Project status
This package is in the very early stage.

[combine]: https://docs.rs/combine/*/combine/